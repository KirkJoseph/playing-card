
#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Clubs = 1, Hearts, Diamonds, Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) // print the rank and suit of the card
{
	
	cout << "The ";

	switch (card.rank)
	{

		case 2: { cout << "Two"; } break;
		case 3: { cout << "Three"; } break;
		case 4: { cout << "Four"; } break;
		case 5: { cout << "Five"; } break;
		case 6: { cout << "Six"; } break;
		case 7: { cout << "Seven"; } break;
		case 8: { cout << "Eight"; } break;
		case 9: { cout << "Nine"; } break;
		case 10: { cout << "Ten"; } break;
		case 11: { cout << "Jack"; } break;
		case 12: { cout << "Queen"; } break;
		case 13: { cout << "King"; } break;
		case 14: { cout << "Ace"; } break;

	}

	cout << " of ";

	switch (card.suit)
	{

		case 1: { cout << "Clubs"; } break;
		case 2: { cout << "Hearts"; } break;
		case 3: { cout << "Diamonds"; } break;
		case 4: { cout << "Spades"; } break;

	}

}

Card HighCard(Card card1, Card card2) // determine card with highest rank - if ranks are equal card1 is returned
{

	if (card1.rank >= card2.rank)
	{
		
		return card1;

	}
	else
	{

		return card2;

	}
}

int main()
{

	Card card1;
	Card card2;

	card1.rank = Four;
	card1.suit = Clubs;

	card2.rank = Three;
	card2.suit = Spades;

	PrintCard(HighCard(card1, card2));

	_getch();
	return 0;
}
